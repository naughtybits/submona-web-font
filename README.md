# submona-web-font

The submona web font project attempts to create a lightweight subset of monafont that can render common Shift_JIS text art in an acceptable manner.

forked from [https://github.com/pera/submona-web-font](https://github.com/pera/submona-web-font)